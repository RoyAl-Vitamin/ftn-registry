# О сервисе

Микросервис переносит в БД данные о калорийности продуктов, взятые с сайта [ars.usda.gov](https://www.ars.usda.gov/northeast-area/beltsville-md-bhnrc/beltsville-human-nutrition-research-center/methods-and-application-of-food-composition-laboratory/mafcl-site-pages/database-resources/) 
в виде `*.csv` фалов. 
А также даёт возможность делать запросы на выборку этих данных с помощью http-запросов.

# Настройка рабочего окружения

Первым действием для запуска приложения необходимо выполнить подъём контейнеров с БД. 
Для этого в проекте есть файл `/ftn-registry/work environment/docker-compose.yml`. 
Подъём осуществляется командой:

```shell
docker-compose down --volumes && docker-compose up -d && exit
```

# Сборка

Jar-файла:

```shell
mvn clean package -DskipTests
```

Docker контейнера:

```shell
docker build -t rvitamin/ftn-registry:v1.0.0 .
```

Что бы отправить собранный образ в docker hub:

```shell
docker push rvitamin/ftn-registry:v1.0.0
```

# Запуск

```shell
java -jar target/ftn-registry-1.0.0.jar
```

В Docker контейнере:

```shell
docker run --name ftn-registry -p 8080:8080 rvitamin/ftn-registry:v1.0.0
```

В k8s:

```bash
kubectl apply -f k8s/deployment.yaml
kubectl apply -f k8s/service.yaml
```

# Swagger

Локальный Swagger доступен по [ссылке](http://localhost:8080/swagger-ui/index.html)

# Ссылки

1. Пример [репозитория](https://github.com/mkjelland/spring-boot-postgres-on-k8s-sample);
2. Тестирование с помощью TestContainers [1](https://testcontainers.com/guides/testing-spring-boot-rest-api-using-testcontainers/), [2](https://testcontainers.com/guides/getting-started-with-testcontainers-for-java/).
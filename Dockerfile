FROM openjdk:21-slim
ENV TZ "Europe/Moscow"
COPY target/ftn-registry-*.jar /opt/app/ftn-registry.jar
WORKDIR /opt/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/app/ftn-registry.jar"]

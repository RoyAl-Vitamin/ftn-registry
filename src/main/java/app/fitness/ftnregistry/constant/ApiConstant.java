package app.fitness.ftnregistry.constant;

public final class ApiConstant {

    private static final String API = "api";
    private static final String VERSION = "v1";
    private static final String BASE_ENDPOINT = "/" + API + "/" + VERSION;

    public static final String FOOD_ATTRIBUTE_ENDPOINT = BASE_ENDPOINT + "/foodAttribute";

    public static final String FOOD_CALORIE_CONVERSION_FACTOR_ENDPOINT = BASE_ENDPOINT + "/foodCalorieConversionFactor";

    public static final String FOOD_ENDPOINT = BASE_ENDPOINT + "/food";
}

package app.fitness.ftnregistry.controller;

import app.fitness.common.FilterRequest;
import app.fitness.common.Page;
import app.fitness.ftnregistry.constant.ApiConstant;
import app.fitness.ftnregistry.service.food.FoodService;
import app.fitness.registry.FoodDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstant.FOOD_ENDPOINT)
public class FoodController {

    private final FoodService foodService;

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get a food by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the food",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = FoodDto.class)) }),
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied")})
    @GetMapping(value = "/getFoodById/{foodId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FoodDto> getFoodById(@Parameter(description = "food id") @PathVariable Long foodId) {
        log.info("GET: /getFoodById/{}", foodId);

        Optional<FoodDto> oFood = foodService.findFoodById(foodId);

        return oFood.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get some foods")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the foods",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = FoodDto.class))) })})
    @PostMapping(value = "/getFood", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<FoodDto>> getFood(@RequestBody FilterRequest filterRequest) {
        log.info("POST: /getFood with param [{}]", filterRequest);

        Page<FoodDto> page = foodService.findFoodByFilter(filterRequest);

        return ResponseEntity.ok(page);
    }
}

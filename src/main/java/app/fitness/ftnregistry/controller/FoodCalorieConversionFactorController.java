package app.fitness.ftnregistry.controller;

import app.fitness.ftnregistry.constant.ApiConstant;
import app.fitness.ftnregistry.service.food_calorie_conversion_factor.FoodCalorieConversionFactorService;
import app.fitness.registry.FoodCalorieConversionFactorDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstant.FOOD_CALORIE_CONVERSION_FACTOR_ENDPOINT)
public class FoodCalorieConversionFactorController {

    private final FoodCalorieConversionFactorService foodCalorieConversionFactorService;

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get a food calorie conversion factor by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the food calorie conversion factor",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = FoodCalorieConversionFactorDto.class)) }),
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied")})
    @GetMapping(value = "/getFoodCalorieConversionFactorById/{foodCalorieConversionFactorId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FoodCalorieConversionFactorDto> getFoodById(@Parameter(description = "food calorie conversion factor id") @PathVariable Long foodCalorieConversionFactorId) {
        log.info("GET: /getFoodCalorieConversionFactorById/{}", foodCalorieConversionFactorId);

        Optional<FoodCalorieConversionFactorDto> oFoodCalorieConversionFactor = foodCalorieConversionFactorService.findFoodCalorieConversionFactorById(foodCalorieConversionFactorId);

        return oFoodCalorieConversionFactor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }
}

package app.fitness.ftnregistry.controller;

import app.fitness.ftnregistry.constant.ApiConstant;
import app.fitness.ftnregistry.service.food_attribute.FoodAttributeService;
import app.fitness.registry.FoodAttributeDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstant.FOOD_ATTRIBUTE_ENDPOINT)
public class FoodAttributeController {

    private final FoodAttributeService foodAttributeService;

    @CrossOrigin(origins = "*")
    @Operation(summary = "Get a food attribute by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the food attribute",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = FoodAttributeDto.class)) }),
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied")})
    @GetMapping(value = "/getFoodAttributeById/{foodAttributeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FoodAttributeDto> getFoodById(@Parameter(description = "food attribute id") @PathVariable Long foodAttributeId) {
        log.info("GET: /getFoodAttributeById/{}", foodAttributeId);

        Optional<FoodAttributeDto> oFoodAttribute = foodAttributeService.getFoodAttributeById(foodAttributeId);

        return oFoodAttribute.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }
}

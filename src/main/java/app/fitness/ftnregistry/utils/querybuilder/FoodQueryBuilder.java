package app.fitness.ftnregistry.utils.querybuilder;

import app.fitness.common.FilterRequest;
import app.fitness.ftnregistry.model.FoodEntity;
import app.fitness.ftnregistry.model.FoodEntity_;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

@UtilityClass
public class FoodQueryBuilder extends CommonQueryBuilder {
    public static Specification<FoodEntity> getSpecification(List<FilterRequest.Filter> filterList) {
        Specification<FoodEntity> spec = getSpec(filterList.get(0));
        for (int i = 1; i < filterList.size(); i++) {
            spec = spec.and(getSpec(filterList.get(i)));
        }
        return spec;
    }

    public Specification<FoodEntity> getSpec(FilterRequest.Filter item) {
        Specification<FoodEntity> specification;
        switch (item.getField()) {
            case FoodEntity_.PUBLICATION_DATE:
                specification = CommonQueryBuilder.getSpecByDate(item);
                break;
            default:
                specification = CommonQueryBuilder.getSpec(item);
        }
        return specification;
    }
}

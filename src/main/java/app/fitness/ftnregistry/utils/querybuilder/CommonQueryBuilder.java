package app.fitness.ftnregistry.utils.querybuilder;

import app.fitness.common.FilterRequest;
import app.fitness.ftnregistry.utils.specification.CommonSpecification;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class CommonQueryBuilder {

    public static Pageable getPageable(FilterRequest.PageRequest pageRequest, List<Sort.Order> orderList) {
        return PageRequest.of(
                pageRequest.getPage(),
                pageRequest.getSize(),
                Sort.by(orderList));
    }

    public static List<Sort.Order> getSortOrder(List<FilterRequest.Sort> items) {
        if (items == null || items.isEmpty()) {
            return new ArrayList<>();
        }

        List<Sort.Order> orderList = new ArrayList<>(items.size());

        for (FilterRequest.Sort item : items) {
            if (item.isDesc()) {
                orderList.add(new Sort.Order(Sort.Direction.DESC, item.getField()));
            } else {
                orderList.add(new Sort.Order(Sort.Direction.ASC, item.getField()));
            }
        }
        return orderList;
    }

    protected static <T> Specification<T> getSpec(FilterRequest.Filter item) {
        Specification<T> specification;

        if (FilterRequest.ComparisonType.IN.equals(item.getComparisonType())) {
            specification = Specification.where(CommonSpecification.inSpec(
                    item.getField(),
                    item.getValues()
            ));
        } else {
            specification = Specification.where(CommonSpecification.eqSpecSuper(item.getField(), item.getValue()));
        }

        return specification;
    }

    protected static <T> Specification<T> getSpecByDate(FilterRequest.Filter item) {
        Specification<T> specification;

        if (FilterRequest.ComparisonType.BETWEEN.equals(item.getComparisonType())) {
            specification = Specification.where(CommonSpecification.dateBetweenSpec(
                    item.getField(),
                    CommonSpecification.getFormattedLocalDate(item.getValues().get(0)),
                    CommonSpecification.getFormattedLocalDate(item.getValues().get(1))
            ));
        } else {
            specification = Specification.where(CommonSpecification.eqSpecSuper(item.getField(), CommonSpecification.getFormattedLocalDate(item.getValue())));
        }

        return specification;
    }
}

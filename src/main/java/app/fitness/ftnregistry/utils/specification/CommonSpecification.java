package app.fitness.ftnregistry.utils.specification;

import app.fitness.ftnregistry.constant.DateConstant;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;

@UtilityClass
public class CommonSpecification {

    public static <T, Y> Specification<Y> eqSpecSuper(String field, T value) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(field), value);
    }

    public static <T> Specification<T> dateBetweenSpec(String fieldName, LocalDate startDate, LocalDate endDate) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.between(
                        root.get(fieldName),
                        startDate,
                        endDate
                );
    }

    public static LocalDate getFormattedLocalDate(String sDate) {
        return LocalDate.parse(sDate, DateConstant.DATE_FORMATTER);
    }

    public static <T> Specification<T> inSpec(String fieldName, List<String> values) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.in(root.get(fieldName)).value(values);
    }
}

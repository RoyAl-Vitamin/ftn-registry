package app.fitness.ftnregistry.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI getOpenAPI(BuildProperties buildProperties) {
        return new OpenAPI()
                .info(new Info().title(buildProperties.getName())
                        .description("Food API sample application")
                        .version(buildProperties.getVersion())
                        .license(new License().name("Apache 2.0").url("https://www.apache.org/licenses/LICENSE-2.0.txt")))
                .externalDocs(new ExternalDocumentation()
                        .description("Repo link")
                        .url("https://bitbucket.org/RoyAl-Vitamin/ftn-registry/src/master"));
    }
}

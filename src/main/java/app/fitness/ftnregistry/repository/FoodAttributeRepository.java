package app.fitness.ftnregistry.repository;

import app.fitness.ftnregistry.model.FoodAttributeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface FoodAttributeRepository extends JpaRepository<FoodAttributeEntity, Long> {
}

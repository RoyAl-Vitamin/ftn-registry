package app.fitness.ftnregistry.repository;

import app.fitness.ftnregistry.model.FoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface FoodRepository extends JpaRepository<FoodEntity, Long>, JpaSpecificationExecutor<FoodEntity> {
}

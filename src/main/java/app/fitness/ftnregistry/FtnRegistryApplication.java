package app.fitness.ftnregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FtnRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FtnRegistryApplication.class, args);
	}

}

package app.fitness.ftnregistry.mapper;

import app.fitness.ftnregistry.model.FoodAttributeEntity;
import app.fitness.registry.FoodAttributeDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FoodAttributeMapper {

    FoodAttributeDto toDto(FoodAttributeEntity source);
}

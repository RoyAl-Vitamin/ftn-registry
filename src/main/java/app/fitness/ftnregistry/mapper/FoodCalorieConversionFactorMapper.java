package app.fitness.ftnregistry.mapper;

import app.fitness.ftnregistry.model.FoodCalorieConversionFactorEntity;
import app.fitness.registry.FoodCalorieConversionFactorDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FoodCalorieConversionFactorMapper {

    FoodCalorieConversionFactorDto toDto(FoodCalorieConversionFactorEntity source);
}

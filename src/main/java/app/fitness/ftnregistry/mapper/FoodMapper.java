package app.fitness.ftnregistry.mapper;

import app.fitness.ftnregistry.model.FoodEntity;
import app.fitness.registry.FoodDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FoodMapper {

    FoodDto toDto(FoodEntity source);
}

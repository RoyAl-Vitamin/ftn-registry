package app.fitness.ftnregistry.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import java.time.LocalDate;

@Getter
@Setter
@Immutable
@Entity(name = "food")
@Table(name = "food")
@NoArgsConstructor
public class FoodEntity {

    @Id
    @SequenceGenerator(name = "food-sequence", sequenceName = "food-sequence", allocationSize = 1)
    @GeneratedValue(generator = "food-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String type;

    @Column
    private String description;

    @Column(name = "food_category_id")
    private Long foodCategoryId;

    @Column(name = "publication_date")
    private LocalDate publicationDate;
}

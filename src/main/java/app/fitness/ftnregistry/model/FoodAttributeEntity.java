package app.fitness.ftnregistry.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

@Getter
@Setter
@Immutable
@Entity(name = "foodAttribute")
@Table(name = "food_attribute")
@NoArgsConstructor
public class FoodAttributeEntity {

    @Id
    @SequenceGenerator(name = "food_attribute-sequence", sequenceName = "food_attribute-sequence", allocationSize = 1)
    @GeneratedValue(generator = "food_attribute-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "fdc_id")
    private String fdcId;

    @Column(name = "seq_num")
    private String seqNum;

    @Column(name = "food_attribute_type_id")
    private String foodAttributeTypeId;

    @Column
    private String name;

    @Column
    private String value;
}

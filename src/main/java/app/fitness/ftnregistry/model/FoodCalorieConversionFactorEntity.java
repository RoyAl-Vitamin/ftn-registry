package app.fitness.ftnregistry.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

@Getter
@Setter
@Immutable
@Entity(name = "foodCalorieConversionFactor")
@Table(name = "food_calorie_conversion_factor")
@NoArgsConstructor
public class FoodCalorieConversionFactorEntity {

    @Id
    @SequenceGenerator(name = "food_calorie_conversion_factor-sequence", sequenceName = "food_calorie_conversion_factor-sequence", allocationSize = 1)
    @GeneratedValue(generator = "food_calorie_conversion_factor-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String protein;

    @Column
    private String fat;

    @Column
    private String carbohydrate;
}

package app.fitness.ftnregistry.service.food_attribute;

import app.fitness.ftnregistry.mapper.FoodAttributeMapper;
import app.fitness.ftnregistry.model.FoodAttributeEntity;
import app.fitness.ftnregistry.repository.FoodAttributeRepository;
import app.fitness.registry.FoodAttributeDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class FoodAttributeServiceImpl implements FoodAttributeService {

    private final FoodAttributeRepository foodAttributeRepository;

    private final FoodAttributeMapper foodAttributeMapper;

    @Override
    public Optional<FoodAttributeDto> getFoodAttributeById(Long foodAttributeId) {
        Optional<FoodAttributeEntity> oFood = foodAttributeRepository.findById(foodAttributeId);
        return oFood.map(foodAttributeMapper::toDto);
    }
}

package app.fitness.ftnregistry.service.food_attribute;

import app.fitness.registry.FoodAttributeDto;

import java.util.Optional;

public interface FoodAttributeService {

    Optional<FoodAttributeDto> getFoodAttributeById(Long foodAttributeId);
}

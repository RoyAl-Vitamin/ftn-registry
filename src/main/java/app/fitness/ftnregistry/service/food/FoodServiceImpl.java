package app.fitness.ftnregistry.service.food;

import app.fitness.common.FilterRequest;
import app.fitness.common.Page;
import app.fitness.ftnregistry.mapper.FoodMapper;
import app.fitness.ftnregistry.model.FoodEntity;
import app.fitness.ftnregistry.repository.FoodRepository;
import app.fitness.ftnregistry.utils.querybuilder.FoodQueryBuilder;
import app.fitness.registry.FoodDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FoodServiceImpl implements FoodService {

    private final FoodMapper foodMapper;

    private final FoodRepository foodRepository;

    @Override
    public Optional<FoodDto> findFoodById(Long foodId) {
        Optional<FoodEntity> oFood = foodRepository.findById(foodId);
        return oFood.map(foodMapper::toDto);
    }

    @Override
    public Page<FoodDto> findFoodByFilter(FilterRequest filterRequest) {
        Page<FoodDto> response = new Page<>();
        org.springframework.data.domain.Page<FoodEntity> all;

        if (filterRequest.getPageRequest().isNeedTotal()) {
            response.setTotal((int) foodRepository.count());
        }

        Pageable pageable = FoodQueryBuilder.getPageable(filterRequest.getPageRequest(), FoodQueryBuilder.getSortOrder(filterRequest.getSortList()));

        if (filterRequest.getFilterList() != null && !filterRequest.getFilterList().isEmpty()) {

            Specification<FoodEntity> specification = FoodQueryBuilder.getSpecification(filterRequest.getFilterList());

            all = foodRepository.findAll(specification, pageable);

            if (filterRequest.getPageRequest().isNeedFilteredTotal()) {
                response.setFilteredTotal((int) foodRepository.count(specification));
            }
        } else {

            all = foodRepository.findAll(pageable);

            if (filterRequest.getPageRequest().isNeedFilteredTotal()) {
                response.setFilteredTotal(response.getTotal() != null ? response.getTotal() : (int) foodRepository.count());
            }
        }

        List<FoodDto> dtoList = all.stream()
                .map(foodMapper::toDto)
                .collect(Collectors.toList());
        response.setContent(dtoList);

        return response;
    }
}

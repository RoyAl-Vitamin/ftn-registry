package app.fitness.ftnregistry.service.food;

import app.fitness.common.FilterRequest;
import app.fitness.common.Page;
import app.fitness.registry.FoodDto;

import java.util.Optional;

public interface FoodService {

    Optional<FoodDto> findFoodById(Long foodId);

    Page<FoodDto> findFoodByFilter(FilterRequest filterRequest);
}

package app.fitness.ftnregistry.service.food_calorie_conversion_factor;

import app.fitness.ftnregistry.mapper.FoodCalorieConversionFactorMapper;
import app.fitness.ftnregistry.model.FoodCalorieConversionFactorEntity;
import app.fitness.ftnregistry.repository.FoodCalorieConversionFactorRepository;
import app.fitness.registry.FoodCalorieConversionFactorDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class FoodCalorieConversionFactorServiceImpl implements FoodCalorieConversionFactorService {

    private final FoodCalorieConversionFactorRepository foodCalorieConversionFactorRepository;

    private final FoodCalorieConversionFactorMapper foodCalorieConversionFactorMapper;

    @Override
    public Optional<FoodCalorieConversionFactorDto> findFoodCalorieConversionFactorById(Long foodCalorieConversionFactorId) {
        Optional<FoodCalorieConversionFactorEntity> oFood = foodCalorieConversionFactorRepository.findById(foodCalorieConversionFactorId);
        return oFood.map(foodCalorieConversionFactorMapper::toDto);
    }
}

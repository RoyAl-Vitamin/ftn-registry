package app.fitness.ftnregistry.service.food_calorie_conversion_factor;

import app.fitness.registry.FoodCalorieConversionFactorDto;

import java.util.Optional;

public interface FoodCalorieConversionFactorService {

    Optional<FoodCalorieConversionFactorDto> findFoodCalorieConversionFactorById(Long foodCalorieConversionFactorId);
}

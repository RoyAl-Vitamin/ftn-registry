package app.fitness.ftnregistry.controller;

import app.fitness.common.FilterRequest;
import app.fitness.common.Page;
import app.fitness.ftnregistry.constant.DateConstant;
import app.fitness.ftnregistry.mapper.FoodMapper;
import app.fitness.ftnregistry.model.FoodEntity;
import app.fitness.registry.FoodDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FoodControllerTests {

    @LocalServerPort
    private Integer port;

    @Autowired
    FoodMapper foodMapper;

    @Autowired
    ObjectMapper mapper;

    private final HttpClient client = HttpClient.newHttpClient();

    public static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15");

    @BeforeAll
    public static void beforeAll() {
        postgres.withInitScript("schema.sql").start();
    }

    @AfterAll
    public static void afterAll() {
        postgres.stop();
    }

    @DynamicPropertySource
    public static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }

    @BeforeEach
    public void setUp() {
//        RestAssured.baseURI = "http://localhost:" + port;
//        foodRepository.count();
    }

    @Test
    public void getFoodById1() {
        FoodEntity foodEntity = new FoodEntity();
        foodEntity.setId(319874L);
        foodEntity.setType("sample_food");
        foodEntity.setDescription("HUMMUS, SABRA CLASSIC");
        foodEntity.setFoodCategoryId(16L);
        foodEntity.setPublicationDate(LocalDate.parse("2019-04-01", DateConstant.DATE_FORMATTER));

        FoodDto foodDto = foodMapper.toDto(foodEntity);

        HttpRequest request = null;
        try {
            request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:" + port + "/api/v1/food/getFoodById/" + foodDto.getId()))
                    .GET()
                    .build();
        } catch (URISyntaxException e) {
            fail(e);
        }

        HttpResponse<String> response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            fail(e);
        }
        FoodDto foodResponse = null;
        try {
            foodResponse = mapper.readValue(response.body(), FoodDto.class);
        } catch (JsonProcessingException e) {
            fail(e);
        }
        assertEquals(foodDto, foodResponse);
    }

    @Test
    public void getFoodById2() {
        FoodEntity foodEntity = new FoodEntity();
        foodEntity.setId(319899L);
        foodEntity.setType("sub_sample_food");
        foodEntity.setDescription("Hummus");
        foodEntity.setFoodCategoryId(16L);
        foodEntity.setPublicationDate(LocalDate.parse("2019-04-01", DateConstant.DATE_FORMATTER));

        FoodDto foodDto = foodMapper.toDto(foodEntity);

        HttpRequest request = null;
        try {
            request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:" + port + "/api/v1/food/getFoodById/" + foodDto.getId()))
                    .GET()
                    .build();
        } catch (URISyntaxException e) {
            fail(e);
        }

        HttpResponse<String> response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            fail(e);
        }
        FoodDto foodResponse = null;
        try {
            foodResponse = mapper.readValue(response.body(), FoodDto.class);
        } catch (JsonProcessingException e) {
            fail(e);
        }
        assertEquals(foodDto, foodResponse);
    }

    @Test
    public void getFood() {
        FilterRequest filterRequest = new FilterRequest();
        List<FilterRequest.Filter> filterList = new ArrayList<>();
        FilterRequest.Filter filter = new FilterRequest.Filter();
        filter.setField("id");
        filter.setComparisonType(FilterRequest.ComparisonType.IN);
        filter.setValues(List.of("319874", "319899"));
        filterList.add(filter);
        filterRequest.setFilterList(filterList);
        List<FilterRequest.Sort> sortList = new ArrayList<>();
        FilterRequest.Sort sort = new FilterRequest.Sort();
        sort.setField("id");
        sort.setDesc(false);
        sortList.add(sort);
        filterRequest.setSortList(sortList);
        FilterRequest.PageRequest pageRequest = new FilterRequest.PageRequest(0, 10);
        pageRequest.setNeedTotal(true);
        pageRequest.setNeedFilteredTotal(true);
        filterRequest.setPageRequest(pageRequest);

        HttpRequest request = null;
        try {
            request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:" + port + "/api/v1/food/getFood"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(filterRequest)))
                    .build();
        } catch (URISyntaxException | JsonProcessingException e) {
            fail(e);
        }

        HttpResponse<String> response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            fail(e);
        }

        Page<FoodDto> foodsResponse = null;
        try {
            foodsResponse = mapper.readValue(response.body(), new TypeReference<Page<FoodDto>>() {});
        } catch (JsonProcessingException e) {
            fail(e);
        }

        assertEquals(20541, foodsResponse.getTotal());
        assertEquals(2, foodsResponse.getFilteredTotal());
        if (foodsResponse.getContent() == null || foodsResponse.getContent().isEmpty()) {
            fail("Список продуктов пуст");
        }

        FoodEntity foodEntity1 = new FoodEntity();
        foodEntity1.setId(319874L);
        foodEntity1.setType("sample_food");
        foodEntity1.setDescription("HUMMUS, SABRA CLASSIC");
        foodEntity1.setFoodCategoryId(16L);
        foodEntity1.setPublicationDate(LocalDate.parse("2019-04-01", DateConstant.DATE_FORMATTER));

        FoodDto foodDto1 = foodMapper.toDto(foodEntity1);

        assertEquals(foodDto1, foodsResponse.getContent().get(0));

        FoodEntity foodEntity2 = new FoodEntity();
        foodEntity2.setId(319899L);
        foodEntity2.setType("sub_sample_food");
        foodEntity2.setDescription("Hummus");
        foodEntity2.setFoodCategoryId(16L);
        foodEntity2.setPublicationDate(LocalDate.parse("2019-04-01", DateConstant.DATE_FORMATTER));

        FoodDto foodDto2 = foodMapper.toDto(foodEntity2);
        assertEquals(foodDto2, foodsResponse.getContent().get(1));
    }
}
